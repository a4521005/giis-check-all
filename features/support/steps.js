const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const puppeteer = require('puppeteer')

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        headless:false,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

Then("it can't be null",{timeout: 2 * 5000},async function(){
    await this.page.waitForSelector('div.data_row', {visible:true})
})


Then("Main Spec should not exist",{timeout: 2 * 5000},async function(){
    const text = await this.page.$eval('div.title1',element => element.textContent)//等待頁面上h1元素出現, $eval是只取得一個元素(querySelector)
    expect(text).to.include('All Specifications')
})

Given("Go to ENUS-HT9060 Spec Page",{timeout: 12 * 5000},async function(){
    await this.page.goto('https://www.benq.com/en-us/projector/cinepro-pro-cinema/ht9060/specifications.html')
})

Given("Go to ENUS-HT8060 Spec Page",{timeout: 12 * 5000},async function(){
    await this.page.goto('https://www.benq.com/en-us/projector/cinepro-pro-cinema/ht8060/specifications.html')
})

Given("Go to ENAP-GL2250 Spec Page",{timeout: 12 * 5000},async function(){
    await this.page.goto('https://www.benq.com/en-ap/monitor/stylish/gl2250/specifications.html')
})

Given("Go to ENUK-PD2500Q Spec Page",{timeout: 12 * 5000},async function(){
    await this.page.goto('https://www.benq.eu/en-uk/monitor/designer/pd2500q/specifications.html')
    await this.page.waitForSelector('button.cookiebtn_close')
    await this.page.click('button.cookiebtn_close')
})
