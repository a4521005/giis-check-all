const puppeteer = require('puppeteer');
const expect = require('chai').expect;

const {click,getText,getCount,shouldExist,waitForText} = require('../../lib/helper')


describe('B2C EN-US Projector - Accessories',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            //executablePath:
            //"./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Accessories Series Page - Check number',async function(){
        //step1:進去B2C各個Projector Series Page
        await page.goto('https://www.benq.com/en-us/projector/accessory.html')
        const accessoriesProjector = "#seriesproducts_copy > div.right > ul.products > ul > li"
        //step2:計算Accessories數量(ex:假設現在是77個, 之後數量如果增加了, 腳本就要改) 
        const countaccessoriesProjector = await getCount(page, accessoriesProjector)
        console.log("Total of Accessories:",countaccessoriesProjector)
        expect(countaccessoriesProjector).to.equal(77)//目前Accessories有77個產品
    })
    it('check each Spec is null or not',async function(){
        //step3:取得URL
        await page.goto('https://www.benq.com/en-us/projector/accessory/wdrt8192/specifications.html')
        //step4:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'div.data_row')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得URL
        await page.goto('https://www.benq.com/en-us/projector/accessory/video-streaming-dongle-qcast-qp01/specifications.html')
        //step4:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'div.data_row')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得URL
        await page.goto('https://www.benq.com/en-us/projector/accessory/wireless-fhd-kit-wdp02/specifications.html')
        //step4:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'div.data_row')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得URL
        await page.goto('https://www.benq.com/en-us/projector/accessory/qcast-mirror-qp20/specifications.html')
        //step4:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'div.data_row')
    })
})
