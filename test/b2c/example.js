const puppeteer = require('puppeteer');
const expect = require('chai').expect;

const {click,getText,getCount,shouldExist,waitForText} = require('../lib/helper')

describe('B2C EN-US Projector - Portable Projectors',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            // executablePath:
            // "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            executablePath:
            "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Series Page - Check number(GV Series)',async function(){
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        await page.waitForSelector('#productlist_copy > div.right > ul.products > ul')
        const gvCount = await getCount(page, '#productlist_copy_cop > div.right > ul.products > ul > li')
        expect(gvCount).to.equal(2)//GV Series目前兩樣商品


        
    })
    it('Go to Series Page - Check number(GS Series)',async function(){
        await page.goto('https://www.benq.com/en-us/projector/portable.html')
        await page.waitForSelector('#productlist_copy_cop > div.right > ul.products > ul')
        const gsCount = await getCount(page, '#productlist_copy_cop > div.right > ul.products > ul > li')
        expect(gvCount).to.equal(2)//GV Series目前兩樣商品

    })
})