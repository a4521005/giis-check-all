const puppeteer = require('puppeteer');
const expect = require('chai').expect;

const {click,getText,getCount,shouldExist,waitForText} = require('../../lib/helper')

describe('B2C check spec test-1',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            headless:false,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(5000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(60000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Main Spec should not exist',async function(){
        await page.goto('https://www.benq.com/en-ap/monitor/stylish/gl2250/specifications.html')
        const text = await page.$eval('div.title1',element => element.textContent)//等待頁面上h1元素出現, $eval是只取得一個元素(querySelector)
        expect(text).to.include('All Specifications')//斷言:h1內容必須為Example Domain
        console.log(text)
    })
    it('Main Spec should not exist',async function(){
        await page.goto('https://www.benq.com/en-us/projector/cinepro-pro-cinema/ht9060/specifications.html')
        const text = await page.$eval('div.title1',element => element.textContent)//等待頁面上h1元素出現, $eval是只取得一個元素(querySelector)
        expect(text).to.include('All Specifications')//斷言:h1內容必須為Example Domain
        console.log(text)
    })
    it('it cannot be null',async function(){
        await page.goto('https://www.benq.eu/en-uk/monitor/designer/pd2500q/specifications.html')
        await page.waitForSelector('button.cookiebtn_close')
        await page.click('button.cookiebtn_close')
        await page.waitForSelector('div.data_row')
    })
})

describe('B2C check spec test-2',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            // executablePath:
            // "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:false,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(30000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(30000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Series Page - Check number',async function(){
        await page.goto('https://www.benq.com/en-us/projector/cinepro-pro-cinema.html')
        const homeCineProProjector = "#seriesproducts_copy_ > div.right > ul.products > ul > li"
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const countHomeCineProProjector = await getCount(page, homeCineProProjector)
        console.log("Total of HomeCinePro Projector:",countHomeCineProProjector)
        expect(countHomeCineProProjector).to.equal(2)//目前Home Projector - CinePro Series有2個產品
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/cinepro-pro-cinema.html')
        var getHomeCineProProjectorURL = "#seriesproducts_copy_ > div.right > ul.products > ul > li:nth-child(1) > a"
        var homeCineProProjectorProductURL = await page.$eval(getHomeCineProProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var homeCineProProjectorURL = homeCineProProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var homeCineProProjectorSpecURL = "https://www.benq.com/"+homeCineProProjectorURL
        console.log(homeCineProProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(homeCineProProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'div.data_row')
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/cinepro-pro-cinema.html')
        var getHomeCineProProjectorURL = "#seriesproducts_copy_ > div.right > ul.products > ul > li:nth-child(2) > a"
        var homeCineProProjectorProductURL = await page.$eval(getHomeCineProProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var homeCineProProjectorURL = homeCineProProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var homeCineProProjectorSpecURL = "https://www.benq.com/"+homeCineProProjectorURL
        console.log(homeCineProProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(homeCineProProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'div.data_row')
    })
})

