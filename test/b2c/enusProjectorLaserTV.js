const puppeteer = require('puppeteer');
const expect = require('chai').expect;

const {click,getText,getCount,shouldExist,waitForText} = require('../../lib/helper')


describe('B2C EN-US Projector - Laser TV Projectors',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            // executablePath:
            // "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to Laser TV Projectors Series Page - Check number',async function(){
        await page.goto('https://www.benq.com/en-us/projector/laser-tv-projector.html')
        const laserTVProjector = "#seriesproducts_copy_ > div.right > ul.products > ul > li"
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const countLaserTVProjector = await getCount(page, laserTVProjector)
        console.log("Total of Laser TV Projector:",countLaserTVProjector)
        expect(countLaserTVProjector).to.equal(1)//目前Laser TV Projector有1個產品
    })
    it('check each Spec is null or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/projector/laser-tv-projector.html')
        var getLaserTVProjectorURL = "#seriesproducts_copy_ > div.right > ul.products > ul > li > a"
        var laserTVProjectorProductURL = await page.$eval(getLaserTVProjectorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var laserTVProjectorURL = laserTVProjectorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var laserTVProjectorSpecURL = "https://www.benq.com/"+laserTVProjectorURL
        console.log(laserTVProjectorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(laserTVProjectorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
})